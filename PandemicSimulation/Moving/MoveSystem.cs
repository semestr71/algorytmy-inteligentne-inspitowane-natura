﻿using System.Drawing;
using PandemicSimulation.Simulation;

namespace PandemicSimulation.Moving;

public class MoveSystem : MoveSystemBase, IMoveSystem
{
    public MoveSystem(int spawnX, int spawnY, Human owner) : base(spawnX, spawnY, owner, 5)
    {
        Destinations = new Point[4];

        Destinations[0] = getSafePoint(spawnX + 5, spawnY + 5);
        Destinations[1] = getSafePoint(spawnX, spawnY + 5);
        Destinations[2] = getSafePoint(spawnX, spawnY);
        Destinations[3] = new Point(spawnX, spawnY);
    }
}