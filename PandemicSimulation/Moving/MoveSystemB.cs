﻿using System.Drawing;
using PandemicSimulation.Simulation;

namespace PandemicSimulation.Moving;

public class MoveSystemB : MoveSystemBase, IMoveSystem
{
    public MoveSystemB(int spawnX, int spawnY, Human owner) : base(spawnX, spawnY, owner, 5)
    {
        Destinations = new Point[4];

        Destinations[0] = new Point(spawnX, spawnY);
        Destinations[1] = new Point(spawnX, spawnY);
        Destinations[2] = getSafePoint(spawnX, spawnY + 10);
        Destinations[3] = getSafePoint(spawnX, spawnY + 10);
    }
}