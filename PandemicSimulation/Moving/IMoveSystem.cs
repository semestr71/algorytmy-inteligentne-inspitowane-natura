﻿namespace PandemicSimulation.Moving;

public interface IMoveSystem
{
    public (int, int) Move();
}