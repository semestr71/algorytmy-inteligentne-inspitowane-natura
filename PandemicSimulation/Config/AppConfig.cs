﻿using System;
using System.Configuration;
using System.Globalization;

namespace PandemicSimulation.Config
{
    public static class AppConfig
    {
        public static int PreferredBackBufferWidth { get { return Convert.ToInt16(GetConfig("PreferredBackBufferWidth")); } }
        public static int PreferredBackBufferHeight { get { return Convert.ToInt16(GetConfig("PreferredBackBufferHeight")); } }
        public static bool IsFixedTimeStep { get { return Convert.ToBoolean(GetConfig("IsFixedTimeStep")); } }
        public static TimeSpan TargetElapsedTime { get { return TimeSpan.FromSeconds(Convert.ToDouble(GetConfig("TargetElapsedTime"), CultureInfo.InvariantCulture)); } }
        public static int NumberOfHumans { get { return Convert.ToInt16(GetConfig("NumberOfHumans")); } }
        public static int NumberOfInitiallyInfectedHumans { get { return Convert.ToInt16(GetConfig("NumberOfInitiallyInfectedHumans")); } }
        public static string AgeSimulationMode { get { return GetConfig("AgeSimulationMode").ToLower(); } }
        public static int HumanAgeValue { get { return Convert.ToInt16(GetConfig("HumanAgeValue")); } }
        public static int MaxHumanAge { get { return Convert.ToInt16(GetConfig("MaxHumanAge")); } }
        public static int MinHumanAge { get { return Convert.ToInt16(GetConfig("MinHumanAge")); } }
        public static string ResistanceSimulationMode { get { return GetConfig("ResistanceSimulationMode").ToLower(); } }
        public static double BasicSusceptibility { get { return Convert.ToDouble(GetConfig("BasicSusceptibility"), CultureInfo.InvariantCulture); } }

        private static string GetConfig(string key)
        {
            if (ConfigurationManager.AppSettings[key] is not null)
            {
                return ConfigurationManager.AppSettings[key].ToString();
            }

            throw new ConfigurationErrorsException($"Given key is not present in the configuration file: {key}");            
        }
    }
}
