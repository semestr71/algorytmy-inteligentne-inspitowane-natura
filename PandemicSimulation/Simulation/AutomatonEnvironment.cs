﻿using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PandemicSimulation.Config;
using PandemicSimulation.Utils;
using Point = Microsoft.Xna.Framework.Point;

namespace PandemicSimulation.Simulation;

public class AutomatonEnvironment
{
    public CellularAutomaton Automaton;
    public GraphicsDevice GraphicsDevice;    
    public Virus Virus;
    public SpriteFont Font;
    public History History = new();
    public bool IsActive = true;
    public RenderTarget2D TargetSimulation;
    public RenderTarget2D TargetPlots;
    public Vector2[][] PlacesOnWindow;
    public Plot Plot;

    public AutomatonEnvironment(int size, GraphicsDevice graphicsDevice, SpriteFont font)
    {
        Automaton = new CellularAutomaton(size, this);
        GraphicsDevice = graphicsDevice;
        Font = font;

        Virus = new Virus
        {
            Contagiousness = 0.9f,
            InfectionRange = 2
        };
        TargetSimulation = new RenderTarget2D(
            GraphicsDevice,
            900,
            900,
            false,
            GraphicsDevice.PresentationParameters.BackBufferFormat,
            DepthFormat.Depth24
        );
        TargetPlots = new RenderTarget2D(
            GraphicsDevice,
            600,
            600,
            false,
            GraphicsDevice.PresentationParameters.BackBufferFormat,
            DepthFormat.Depth24
        );
        Plot = new Plot(GraphicsDevice, new Point(900, 600), Font)
        {
            Position = new Vector2(275, 775),
            MaxValue = AppConfig.NumberOfHumans,
            Type = Plot.PlotType.Line
        };
    }

    public void Initialize(int xPixels, int yPixels, int offset)
    {
        Automaton.Initialize();
        FillPlacesOnWindow(xPixels, yPixels, offset);
    }

    private void FillPlacesOnWindow(int xPixels, int yPixels, int offset)
    {
        var pixelsPerWidthAxis = (float)(xPixels - 2 * offset) / Automaton.Size;
        var pixelsPerHeightAxis = (float)(yPixels - 2 * offset) / Automaton.Size;

        PlacesOnWindow = new Vector2[Automaton.Size][];
        for (var i = 0; i < Automaton.Size; i++)
        {
            PlacesOnWindow[i] = new Vector2[Automaton.Size];
            for (var j = 0; j < Automaton.Size; j++)
                PlacesOnWindow[i][j] = new Vector2(i * pixelsPerHeightAxis + offset, j * pixelsPerWidthAxis + offset);
        }
    }

    public void Step()
    {
        if (!IsActive) return;
        Automaton.Step();
        UpdateHistory();
    }

    public void Draw(Button pauseButton, Button stepButton)
    {
        DrawSimulationWindow();
        DrawPlotWindow();
        DrawAllWindowsAndButtons(pauseButton, stepButton);
    }

    private void DrawSimulationWindow()
    {
        GraphicsDevice.SetRenderTarget(TargetSimulation);
        GraphicsDevice.Clear(Color.Black);
        var spriteBatch = new SpriteBatch(GraphicsDevice);
        spriteBatch.Begin();

        foreach (var human in Automaton.Humans.Values)
            spriteBatch.Draw(
                human.GetTexture(),
                PlacesOnWindow[human.PosX][human.PosY],
                null,
                Color.White,
                0f,
                human.GetTextureSize(),
                Vector2.One,
                SpriteEffects.None,
                0f
            );

        spriteBatch.End();
    }

    private void DrawPlotWindow()
    {
        GraphicsDevice.SetRenderTarget(TargetPlots);
        GraphicsDevice.Clear(Color.Black);
        var spriteBatch = new SpriteBatch(GraphicsDevice);
        spriteBatch.Begin();

        Plot.Draw(History.Healthy, Color.Green, spriteBatch);
        Plot.Draw(History.Infected, Color.Yellow, spriteBatch);
        Plot.Draw(History.Immune, Color.RoyalBlue, spriteBatch);
        Plot.Draw(History.Dead, Color.Crimson, spriteBatch);
        Plot.Draw(History.Undefined, Color.Pink, spriteBatch);

        spriteBatch.DrawString(Font, $"HEALTHY:   {History.Healthy.Last()}", new Vector2(400, 35), Color.Green);
        spriteBatch.DrawString(Font, $"INFECTED:  {History.Infected.Last()}", new Vector2(400, 55), Color.Yellow);
        spriteBatch.DrawString(Font, $"IMMUNE:    {History.Immune.Last()}", new Vector2(400, 75), Color.RoyalBlue);
        spriteBatch.DrawString(Font, $"DEAD:      {History.Dead.Last()}", new Vector2(400, 95), Color.Crimson);
        spriteBatch.DrawString(Font, $"UNDEFINED: {History.Undefined.Last()}", new Vector2(400,  115), Color.Pink);

        spriteBatch.End();
    }

    private void DrawAllWindowsAndButtons(Button pauseBtn, Button stepBtn)
    {
        GraphicsDevice.SetRenderTarget(null);
        GraphicsDevice.Clear(new Color(100, 50, 50));
        var spriteBatch = new SpriteBatch(GraphicsDevice);
        spriteBatch.Begin();

        spriteBatch.Draw(TargetSimulation, new Rectangle(50, 50, 800, 800), Color.White);
        spriteBatch.Draw(TargetPlots, new Rectangle(900, 100, 600, 700), Color.White);
        pauseBtn.Draw(spriteBatch);
        stepBtn.Draw(spriteBatch);

        spriteBatch.End();
    }

    private void UpdateHistory()
    {
        History.Healthy.Add(Automaton.Humans.Values.Count(h => h.Status == Status.Healthy));
        History.Infected.Add(Automaton.Humans.Values.Count(h => h.Status == Status.Infected));
        History.Immune.Add(Automaton.Humans.Values.Count(h => h.Status == Status.Immune));
        History.Dead.Add(Automaton.Humans.Values.Count(h => h.Status == Status.Dead));
        History.Undefined.Add(Automaton.Humans.Values.Count(h => h.Status == Status.Undefined));
    }
}