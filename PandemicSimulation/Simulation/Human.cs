﻿using System;
using System.Data;
using System.Numerics;
using Microsoft.Xna.Framework.Graphics;
using PandemicSimulation.Moving;
using PandemicSimulation.Utils;

namespace PandemicSimulation.Simulation;

public class Human
{
    private static int _highestId = 1;

    private readonly float _chanceToRecover = 0.03f;
    private readonly Random _random;
    public float Age;

    public int Id;
    public IMoveSystem MoveSystem;
    public CellularAutomaton Owner;
    public int PosX, PosY;
    public double Resistance;
    public double FatalityRate;
    public Status Status;

    public Human(CellularAutomaton owner, IMoveSystem moveSystem = null)
    {
        Owner = owner;
        Id = _highestId++;
        Status = Status.Healthy;
        MoveSystem = moveSystem;

        _random = new Random();
    }

    public void Step()
    {
        if (Status == Status.Dead) return;

        if (Status == Status.Infected && ShouldDie())
        {
            Die();
            return;
        }

        if (Status == Status.Infected)
        {
            TryRecover();
        }
        else if (Status == Status.Healthy || Status == Status.Immune)
        {
            Resistance *= 0.999;
            UpdateStatus();
        }

        TryMove();
    }

    public Texture2D GetTexture()
    {
        return Status.GetTexture();
    }

    public Vector2 GetTextureSize()
    {
        var texture = Status.GetTexture();

        return new Vector2(texture.Width, texture.Height);
    }

    private void Die()
    {
        Owner.KillHuman(Id);
    }
    
    private void TryRecover()
    {
        var random = new Random();
        if (random.NextDouble() > _chanceToRecover) return;
        Resistance *= 1.2;
        UpdateStatus();
    }

    private void TryMove()
    {
        int moveX, moveY;
        int oldX = PosX, oldY = PosY;
        if (MoveSystem == null)
        {
            var random = new Random();

            moveX = random.Next(-1, 2);
            moveY = random.Next(-1, 2);

            PosX = Math.Min(Math.Max(PosX + moveX, 0), Owner.Size - 1);
            PosY = Math.Min(Math.Max(PosY + moveY, 0), Owner.Size - 1);
        }
        else
        {
            (moveX, moveY) = MoveSystem.Move();
            PosX = Math.Min(Math.Max(PosX + moveX, 0), Owner.Size - 1);
            PosY = Math.Min(Math.Max(PosY + moveY, 0), Owner.Size - 1);
        }

        Owner.moveHuman(this, oldX, oldY);
    }

    private bool ShouldDie()
    {
        return _random.Next() < Int32.MaxValue * FatalityRate / 100; // 100 tymczasowo
    }

    private void UpdateStatus()
    {
        Status = Resistance >= 1 ? Status.Immune : Status.Healthy;
    }
}