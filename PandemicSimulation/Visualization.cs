﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PandemicSimulation.Config;
using PandemicSimulation.Simulation;
using PandemicSimulation.Utils;

namespace PandemicSimulation;

public class Visualization : Game
{
    private readonly GraphicsDeviceManager _graphics;

    private Button _buttonPause;
    private Button _buttonStep;
    private AutomatonEnvironment _simulation;

    public Visualization()
    {
        _graphics = new GraphicsDeviceManager(this);
        _graphics.PreferredBackBufferWidth = AppConfig.PreferredBackBufferWidth;
        _graphics.PreferredBackBufferHeight = AppConfig.PreferredBackBufferHeight;

        Content.RootDirectory = "Content";
        IsMouseVisible = true;
        IsFixedTimeStep = AppConfig.IsFixedTimeStep;
        TargetElapsedTime = AppConfig.TargetElapsedTime;
    }

    protected override void Initialize()
    {
        // TODO: create 1 common Random and use it everywhere
        // TODO: once we have config split this code into separate methods (Initialize and LoadContent)
        Window.Position = new Point(120, 120);

        // TODO: move this basic font somewhere
        var font = Content.Load<SpriteFont>("PlotFont");

        // TODO: move these buttons somewhere (maybe use Button with static array to store them together)
        _buttonPause = new Button(
            Content.Load<Texture2D>("button"),
            font,
            new Vector2(0, 0),
            () => _simulation.IsActive = !_simulation.IsActive
        );
        _buttonStep = new Button(
            Content.Load<Texture2D>("button1"),
            font,
            new Vector2(100, 0),
            () =>
            {
                _simulation.IsActive = true;
                _simulation.Step();
                _simulation.IsActive = false;
            });

        _simulation = new AutomatonEnvironment(100, GraphicsDevice, font);
        _simulation.Initialize(900, 900, 50);

        base.Initialize();
    }

    protected override void LoadContent()
    {
        StatusExtentions.Textures = new Dictionary<Status, Texture2D>
        {
            { Status.Healthy, Content.Load<Texture2D>("Healthy") },
            { Status.Infected, Content.Load<Texture2D>("Infected") },
            { Status.Immune, Content.Load<Texture2D>("Immune") },
            { Status.Dead, Content.Load<Texture2D>("Dead") },
            { Status.Undefined, Content.Load<Texture2D>("Undefined") },
        };
    }

    protected override void Update(GameTime gameTime)
    {
        if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
            Keyboard.GetState().IsKeyDown(Keys.Escape))
            Exit();

        _simulation.Step();
        _buttonPause.Update();
        _buttonStep.Update();

        base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime)
    {
        _simulation.Draw(_buttonPause, _buttonStep);

        base.Draw(gameTime);
    }
}