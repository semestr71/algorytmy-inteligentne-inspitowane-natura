﻿namespace PandemicSimulation.Interfaces
{
    public interface IResistanceCalculator
    {
        double Calculate(int age);
    }
}
