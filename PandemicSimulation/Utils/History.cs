﻿using System.Collections.Generic;

namespace PandemicSimulation.Utils;

public class History
{
    public readonly List<float> Healthy;
    public readonly List<float> Infected;
    public readonly List<float> Immune;
    public readonly List<float> Dead;
    public readonly List<float> Undefined;

    public History(int maxSize = 1000)
    {
        Healthy = new List<float>(maxSize);
        Infected = new List<float>(maxSize);
        Immune = new List<float>(maxSize);
        Dead = new List<float>(maxSize);
        Undefined = new List<float>(maxSize);
    }

    // public void Update(int healthy, int infected, int recovered)
    // {
    //     if Healthy.Count 
    // }
}