﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PandemicSimulation.Utils;

public class Plot
{
    public enum PlotType
    {
        Line,
        Fill
    }

    private readonly BasicEffect _effect;
    private readonly SpriteFont _font;
    private short[] _lineListIndices;

    private Vector2 _scale = new(1.0f, 1.0f);
    private short[] _triangleStripIndices;

    public Plot(GraphicsDevice graphicsDevice, Point size, SpriteFont font)
    {
        _font = font;
        _effect = new BasicEffect(graphicsDevice);
        _effect.View = Matrix.CreateLookAt(Vector3.Backward, Vector3.Zero, Vector3.Up);
        _effect.Projection = Matrix.CreateOrthographicOffCenter(0, graphicsDevice.Viewport.Width,
            graphicsDevice.Viewport.Height, 0, 1.0f, 1000.0f);
        _effect.World = Matrix.Identity;

        _effect.VertexColorEnabled = true;

        MaxValue = 1;
        Size = size;
        if (size.Y <= 0)
            Size = new Point(size.X, 1);
        if (size.X <= 0)
            Size = new Point(1, size.Y);

        Type = PlotType.Line;
    }

    public PlotType Type { get; set; }
    public Vector2 Position { get; set; }

    /// <summary>
    ///     The size of the graph.
    ///     The graph values will be scaled horizontally to fill width (Size.X)
    ///     Vertically, the values will be scaled based on MaxValue property, where the position of the value that is equal to
    ///     MaxValue will be Size.Y
    /// </summary>
    public Point Size { get; set; }

    /// <summary>
    ///     Determines the vertical scaling of the graph.
    ///     The value that is equal to MaxValue will be displayed at the top of the graph (at point Size.Y)
    /// </summary>
    public float MaxValue { get; set; }

    private void UpdateWorld()
    {
        _effect.World = Matrix.CreateScale(_scale.X, _scale.Y, 1.0f)
                        * Matrix.CreateRotationX(MathHelper
                            .Pi) //flips the graph so that the higher values are above. Makes bottom left the graph origin.
                        * Matrix.CreateTranslation(new Vector3(Position, 0));
    }

    private void DrawAxes(int lengthX, int lengthY, SpriteBatch spriteBatch)
    {
        var axisColor = Color.White;
        var labelsColor = Color.White;
        DrawLineList(new[]
        {
            new VertexPositionColor(new Vector3(0, 0, 0), axisColor),
            new VertexPositionColor(new Vector3(lengthX * 1.1f, 0, 0), axisColor)
        });
        DrawLineList(new[]
        {
            new VertexPositionColor(new Vector3(0, 0, 0), axisColor),
            new VertexPositionColor(new Vector3(0, lengthY * 1.1f, 0), axisColor)
        });
        spriteBatch.DrawString(
            _font,
            "TIME",
            new Vector2(Position.X, Position.Y),
            labelsColor,
            0,
            new Vector2(-10, 150),
            new Vector2(1, 1),
            SpriteEffects.None,
            0f
        );
        spriteBatch.DrawString(
            _font,
            "POPULATION",
            new Vector2(Position.X, Position.Y),
            labelsColor,
            1.5f * 3.14f,
            new Vector2(-350, 200),
            new Vector2(1, 1),
            SpriteEffects.None,
            0f
        );
    }

    /// <summary>
    ///     Draws the values in given order, with specific color for each value
    /// </summary>
    /// <param name="values">Value/color pairs to draw, in order from left to right</param>
    public void Draw(List<Tuple<float, Color>> values)
    {
        if (values.Count < 2) return;

        //creates scaling (for the transformation) based on the number of points to draw
        var xScale = Size.X / (float)values.Count;
        var yScale = Size.Y / MaxValue;

        _scale = new Vector2(xScale, yScale);
        UpdateWorld();

        //different point lists for different types of graphs
        if (Type == PlotType.Line)
        {
            var pointList = new VertexPositionColor[values.Count];
            for (var i = 0; i < values.Count; i++)
                pointList[i] =
                    new VertexPositionColor(new Vector3(i, values[i].Item1 < MaxValue ? values[i].Item1 : MaxValue, 0),
                        values[i].Item2);

            DrawLineList(pointList);
        }
        else if (Type == PlotType.Fill)
        {
            var pointList = new VertexPositionColor[values.Count * 2];
            for (var i = 0; i < values.Count; i++)
            {
                //The vertices are created so that the triangles are inverted (back facing). When rotated they will become front facing.
                //This is done to avoid changing rasterizer state to CullMode.CullClockwiseFace.
                pointList[i * 2 + 1] = new VertexPositionColor(
                    new Vector3(i, values[i].Item1 < MaxValue ? values[i].Item1 : MaxValue, 0), values[i].Item2);
                pointList[i * 2] = new VertexPositionColor(new Vector3(i, 0, 0), values[i].Item2);
            }

            DrawTriangleStrip(pointList);
        }
    }

    public void Draw(List<float> values, Color color, SpriteBatch spriteBatch)
    {
        if (values.Count < 2)
            return;

        var xScale = Size.X / (float)values.Count;
        var yScale = Size.Y / MaxValue;

        _scale = new Vector2(xScale, yScale);
        UpdateWorld();

        if (Type == PlotType.Line)
        {
            var pointList = new VertexPositionColor[values.Count];
            for (var i = 0; i < values.Count; i++)
                pointList[i] = new VertexPositionColor(new Vector3(i, values[i] < MaxValue ? values[i] : MaxValue, 0),
                    color);

            DrawLineList(pointList);
            DrawAxes(pointList.Length, (int)MaxValue, spriteBatch);
        }
        else if (Type == PlotType.Fill)
        {
            var pointList = new VertexPositionColor[values.Count * 2];
            for (var i = 0; i < values.Count; i++)
            {
                pointList[i * 2 + 1] =
                    new VertexPositionColor(new Vector3(i, values[i] < MaxValue ? values[i] : MaxValue, 0), color);
                pointList[i * 2] = new VertexPositionColor(new Vector3(i, 0, 0), color);
            }

            DrawTriangleStrip(pointList);
            DrawAxes(pointList.Length, (int)MaxValue, spriteBatch);
        }
    }

    private void DrawLineList(VertexPositionColor[] pointList)
    {
        //indices updated only need to be updated when the number of points has changed
        if (_lineListIndices == null || _lineListIndices.Length != pointList.Length * 2 - 2)
        {
            _lineListIndices = new short[pointList.Length * 2 - 2];
            for (var i = 0; i < pointList.Length - 1; i++)
            {
                _lineListIndices[i * 2] = (short)i;
                _lineListIndices[i * 2 + 1] = (short)(i + 1);
            }
        }

        foreach (var pass in _effect.CurrentTechnique.Passes)
        {
            pass.Apply();
            _effect.GraphicsDevice.DrawUserIndexedPrimitives(
                PrimitiveType.LineList,
                pointList,
                0,
                pointList.Length,
                _lineListIndices,
                0,
                pointList.Length - 1
            );
        }
    }

    private void DrawTriangleStrip(VertexPositionColor[] pointList)
    {
        if (_triangleStripIndices == null || _triangleStripIndices.Length != pointList.Length)
        {
            _triangleStripIndices = new short[pointList.Length];
            for (var i = 0; i < pointList.Length; i++)
                _triangleStripIndices[i] = (short)i;
        }

        foreach (var pass in _effect.CurrentTechnique.Passes)
        {
            pass.Apply();
            _effect.GraphicsDevice.DrawUserIndexedPrimitives(
                PrimitiveType.TriangleStrip,
                pointList,
                0,
                pointList.Length,
                _triangleStripIndices,
                0,
                pointList.Length - 2
            );
        }
    }
}