﻿using System;
using System.ComponentModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PandemicSimulation.Utils;

public class Button : Component
{
    public readonly Action Action;
    private MouseState _currentMouse;
    private readonly SpriteFont _font;
    private bool _isHovering;
    private MouseState _previousMouse;
    private readonly Texture2D _texture;

    public Button(Texture2D texture, SpriteFont font, Vector2 position, Action action)
    {
        _texture = texture;
        _font = font;
        Position = position;
        Action = action;
        PenColour = Color.Black;
    }

    public Color PenColour { get; set; }
    public Vector2 Position { get; set; }

    public Rectangle Rectangle
    {
        get
        {
            var x = (int)Position.X;
            var y = (int)Position.Y;
            return new Rectangle(x, y, _texture.Width, _texture.Height);
        }
    }

    public string Text { get; set; }

    public void Draw(SpriteBatch spriteBatch)
    {
        var colour = Color.White;

        if (_isHovering)
            colour = Color.Gray;

        spriteBatch.Draw(_texture, Rectangle, colour);

        if (string.IsNullOrEmpty(Text)) return;
        var x = Rectangle.X + Rectangle.Width / 2 - _font.MeasureString(Text).X / 2;
        var y = Rectangle.Y + Rectangle.Height / 2 - _font.MeasureString(Text).Y / 2;

        spriteBatch.DrawString(_font, Text, new Vector2(x, y), PenColour);
    }

    public void Update()
    {
        _previousMouse = _currentMouse;
        _currentMouse = Mouse.GetState();

        var mouseRectangle = new Rectangle(_currentMouse.X, _currentMouse.Y, 1, 1);

        _isHovering = false;

        if (!mouseRectangle.Intersects(Rectangle)) return;
        _isHovering = true;

        if (_currentMouse.LeftButton == ButtonState.Released && _previousMouse.LeftButton == ButtonState.Pressed)
            Action.Invoke();
    }
}