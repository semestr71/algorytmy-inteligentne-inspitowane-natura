﻿using System;
using System.Collections.Generic;
using System.Drawing;
using PandemicSimulation.Config;
using PandemicSimulation.Moving;
using PandemicSimulation.Simulation;
using PandemicSimulation.VirusSpreading;

namespace PandemicSimulation.Utils;

public class CellularAutomaton
{
    private int[][] _cells;
    public int Size { get; }
    public AutomatonEnvironment Owner;
    public Dictionary<int, Human> Humans = new();
    private readonly int NumberOfHumans = AppConfig.NumberOfHumans;

    public CellularAutomaton(int size, AutomatonEnvironment parent)
    {
        Owner = parent;
        Size = size;
    }

    public void Initialize()
    {
        FillCells();
        SpawnHumans();
    }

    private void FillCells()
    {
        _cells = new int[Size][];

        for (var i = 0; i < Size; i++)
        {
            _cells[i] = new int[Size];
            Array.Fill<int>(_cells[i], (int)Status.Undefined);
        }
    }

    private void SpawnHumans()
    {
        var random = new Random();
        var humanCreator = new HumanCreator(this, Size);

        for (var i = 0; i < NumberOfHumans; i++)
        {
            var human = humanCreator.Create();
            var moveSystemIndex = random.Next() % 6;

            switch (moveSystemIndex)
            {
                case 0:
                    human.MoveSystem = new MoveSystem(human.PosX, human.PosY, human);
                    break;
                case 1:
                    human.MoveSystem = new MoveSystemB(human.PosX, human.PosY, human);
                    break;
                case 2:
                    human.MoveSystem = new MoveSystemC(human.PosX, human.PosY, human);
                    break;
                case 3:
                    human.MoveSystem = new MoveSystemD(human.PosX, human.PosY, human);
                    break;
                case 4:
                    human.MoveSystem = new MoveSystemE(human.PosX, human.PosY, human);
                    break;
                case 5:
                    human.MoveSystem = new MoveSystemF(human.PosX, human.PosY, human);
                    break;
            }

            if (i < AppConfig.NumberOfInitiallyInfectedHumans)
            {
                human.Status = Status.Infected;
            }

            Humans.Add(human.Id, human);
            _cells[human.PosX][human.PosY] = human.Id;
        }
    }

    public Point getSafePoint(int x, int y)
    {
        if (x < 0)
            x = 0;
        else if (x >= Size) x = Size - 1;

        if (y < 0)
            y = 0;
        else if (y >= Size) y = Size - 1;

        return new Point(x, y);
    }

    public void moveHuman(Human human, int OldX, int OldY)
    {
        _cells[OldX][OldY] = (int)Status.Undefined;
        _cells[human.PosX][human.PosY] = human.Id;
    }

    public void Step()
    {
        var infectionRange = Owner.Virus.InfectionRange;

        foreach (var human in Humans.Values)
        {
            human.Step();

            if (human.Status != Status.Infected && human.Status != Status.Dead) continue;

            var startX = Math.Max(human.PosX - infectionRange, 0);
            var endX = Math.Min(human.PosX + infectionRange, Size - 1);
            var startY = Math.Max(human.PosY - infectionRange, 0);
            var endY = Math.Min(human.PosY + infectionRange, Size - 1);
            for (var i = startX; i <= endX; i++)
            for (var j = startY; j <= endY; j++)
                if (_cells[i][j] != (int)Status.Undefined)
                    Owner.Virus.TryInfect(Humans[_cells[i][j]]);
        }
    }

    public void KillHuman(int id)
    {
        var human = Humans[id];
        human.Status = Status.Dead;
        _cells[human.PosX][human.PosY] = (int)Status.Dead;
    }
}