﻿using PandemicSimulation.Config;
using PandemicSimulation.Interfaces;
using System;

namespace PandemicSimulation.VirusSpreading
{
    public class AgeCalculatorRandom : IAgeCalculator
    {
        private Random RandomGenerator = new Random();
        private int _maxAge = Math.Max(AppConfig.MinHumanAge, AppConfig.MaxHumanAge);
        private int _minAge = Math.Min(AppConfig.MinHumanAge, AppConfig.MaxHumanAge);

        public int Calculate() => RandomGenerator.Next(_minAge, _maxAge);
    }
}
