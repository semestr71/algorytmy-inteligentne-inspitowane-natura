﻿using PandemicSimulation.Config;
using PandemicSimulation.Interfaces;

namespace PandemicSimulation.VirusSpreading
{
    public class AgeCalculatorConst : IAgeCalculator
    {
        private readonly int Age = AppConfig.HumanAgeValue;

        public int Calculate() => Age;
    }
}
