﻿using PandemicSimulation.Config;
using PandemicSimulation.Interfaces;
using PandemicSimulation.Simulation;
using PandemicSimulation.Utils;
using System;

namespace PandemicSimulation.VirusSpreading
{
    public class HumanCreator
    {
        private CellularAutomaton _automaton;
        private int _size;
        private Random _random;
        private IAgeCalculator _ageCalculator;
        private IResistanceCalculator _resistanceCalculator;
        private FatalityRateCalculator _fatalityRateCalculator;

        public HumanCreator(CellularAutomaton automaton, int size)
        {
            _automaton = automaton;
            _size = size;
            _random = new Random();
            _ageCalculator = AppConfig.AgeSimulationMode == "random" ? new AgeCalculatorRandom() : new AgeCalculatorConst();
            _resistanceCalculator = AppConfig.ResistanceSimulationMode == "active" ? new ResistanceCalculatorActive() : new ResistanceCalculatorConst();
            _fatalityRateCalculator = new FatalityRateCalculator();
        }

        public Human Create()
        {
            var age = _ageCalculator.Calculate();

            return new Human(_automaton)
            {
                PosX = _random.Next(_size),
                PosY = _random.Next(_size),
                Age = age,
                Resistance = _resistanceCalculator.Calculate(age),
                FatalityRate = _fatalityRateCalculator.Calculate(age)
            };
        }
    }
}
